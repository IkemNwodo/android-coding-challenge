package ikem.nwodo.carddetailfinder.binding

import android.view.View
import android.widget.ProgressBar
import androidx.constraintlayout.solver.widgets.ConstraintHorizontalLayout
import androidx.databinding.BindingAdapter

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibility")
    fun setProgressBarVisibility(progressBar: ProgressBar, loading: Boolean) {
                if (loading) {
                    progressBar.visibility = View.VISIBLE
                } else{
                    progressBar.visibility = View.INVISIBLE
                }
    }

    @JvmStatic
    @BindingAdapter("linearVisibility")
    fun setLinearLayoutVisibility(view: View, isError: Boolean) {
        if (isError) {
            view.visibility = View.VISIBLE
        } else{
            view.visibility = View.INVISIBLE
        }
    }
}