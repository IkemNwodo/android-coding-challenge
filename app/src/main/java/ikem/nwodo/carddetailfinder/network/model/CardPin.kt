package ikem.nwodo.carddetailfinder.network.model

data class CardPin(val pin: Int)