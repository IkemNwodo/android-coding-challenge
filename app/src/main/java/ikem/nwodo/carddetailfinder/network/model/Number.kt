package ikem.nwodo.carddetailfinder.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Number {

    @SerializedName("length")
    @Expose
    var length: Int? = null
    @SerializedName("luhn")
    @Expose
    var luhn: Boolean? = null

}