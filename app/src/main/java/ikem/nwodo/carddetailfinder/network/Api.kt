package ikem.nwodo.carddetailfinder.network

import ikem.nwodo.carddetailfinder.network.model.Result
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET("{pin}")
    suspend fun loadCardDetail(@Path("pin") pin: Int): Result
}