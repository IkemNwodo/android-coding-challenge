package ikem.nwodo.carddetailfinder.network.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Result constructor(
    @SerializedName("number")
    @Expose
    var number: Number? = null,
    @SerializedName("scheme")
    @Expose
    var scheme: String? = null,
    @SerializedName("type")
    @Expose
    var type: String? = null,
    @SerializedName("brand")
    @Expose
    var brand: String? = null,
    @SerializedName("prepaid")
    @Expose
    var prepaid: Boolean? = null,
    @SerializedName("country")
    @Expose
    var country: Country? = null,
    @SerializedName("bank")
    @Expose
    var bank: Bank? = null
)
