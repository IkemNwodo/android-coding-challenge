package ikem.nwodo.carddetailfinder.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import dagger.android.support.DaggerFragment
import ikem.nwodo.carddetailfinder.R
import ikem.nwodo.carddetailfinder.Repository
import ikem.nwodo.carddetailfinder.databinding.FragmentDetailBinding
import ikem.nwodo.carddetailfinder.network.model.CardPin
import ikem.nwodo.carddetailfinder.network.model.Result
import kotlinx.coroutines.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : DaggerFragment() {

    @Inject
    lateinit var repository: Repository

    lateinit var binding: FragmentDetailBinding

    private val args: DetailFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =  DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)

        return binding.root
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun showDetail() {
        GlobalScope.launch(Dispatchers.Main) {
            repository.cardPin = CardPin(args.cardPin)
            val cardDetail = repository.fetchCardDetail()
            binding.result = cardDetail
        }
    }
    @ExperimentalCoroutinesApi
    @FlowPreview
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.card_detail)

        showDetail()

    }


}
