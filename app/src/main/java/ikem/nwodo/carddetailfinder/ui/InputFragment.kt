package ikem.nwodo.carddetailfinder.ui


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import dagger.android.support.DaggerFragment
import ikem.nwodo.carddetailfinder.R
import ikem.nwodo.carddetailfinder.databinding.FragmentInputBinding

/**
 * A simple [Fragment] subclass.
 */
class InputFragment : DaggerFragment(), View.OnClickListener {

    lateinit var binding: FragmentInputBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_input, container, false)

        binding.checkButton.setOnClickListener(this)
        binding.checkButton.isEnabled = false
        binding.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.checkButton.isEnabled = binding.input.text != null
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


        return binding.root
    }

    override fun onClick(v: View?) {
        if (binding.input.text != null){
            val action = InputFragmentDirections.actionInputFragmentToDetailFragment((binding.input.text.toString().toInt()))
            findNavController().navigate(action)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.card_input)

    }

}
