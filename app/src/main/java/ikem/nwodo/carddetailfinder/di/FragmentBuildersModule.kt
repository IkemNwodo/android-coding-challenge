package ikem.nwodo.carddetailfinder.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ikem.nwodo.carddetailfinder.ui.DetailFragment
import ikem.nwodo.carddetailfinder.ui.InputFragment

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeInputFragment(): InputFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailFragment(): DetailFragment
}