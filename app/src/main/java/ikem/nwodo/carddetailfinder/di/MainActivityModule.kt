package ikem.nwodo.carddetailfinder.di

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ikem.nwodo.carddetailfinder.ui.MainActivity

@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(
        modules = [FragmentBuildersModule::class]
    )
    abstract fun contributeMainActivity() : MainActivity
}
