package ikem.nwodo.carddetailfinder.di

import android.app.Application
import javax.inject.Singleton

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import ikem.nwodo.carddetailfinder.CardApplication


@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    MainActivityModule::class])
internal interface CardComponent : AndroidInjector<CardApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): CardComponent
    }
}