package ikem.nwodo.carddetailfinder

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import ikem.nwodo.carddetailfinder.di.DaggerCardComponent

class CardApplication : DaggerApplication(){
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerCardComponent.builder().application(this).build()
    }

}