package ikem.nwodo.carddetailfinder

import com.dropbox.android.external.store4.*
import ikem.nwodo.carddetailfinder.network.Api
import ikem.nwodo.carddetailfinder.network.model.CardDetail
import ikem.nwodo.carddetailfinder.network.model.CardPin
import ikem.nwodo.carddetailfinder.network.model.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * uses the store 4 library by dropbox
 */
@Singleton
class Repository @Inject constructor(val api: Api){

    var cardPin: CardPin = CardPin(0)

    @ExperimentalCoroutinesApi
    @FlowPreview
    private val store = StoreBuilder
        .fromNonFlow<Int, Result> { api.loadCardDetail(cardPin.pin) }
        .cachePolicy(
            MemoryPolicy
                .builder()
                .setExpireAfterWrite(10)
                .setExpireAfterTimeUnit(TimeUnit.MINUTES)
                .build()

        )
        .build()

    @ExperimentalCoroutinesApi
    @FlowPreview
     suspend fun fetchCardDetail(): Result{
        /**var cardDetail = CardDetail()
        val result = store.stream(StoreRequest.cached(key = cardPin.pin, refresh = true)).collect{ response ->
            when (response){
                is StoreResponse.Data -> cardDetail.result = response.value
                //is StoreResponse.Error -> cardDetail.error = true
                //is StoreResponse.Loading -> cardDetail.loading = true
            }
        }*/
        return store.get(cardPin.pin)
    }
}
