# Android Coding Challenge - Card Detail Finder
App shows card's details when a card's number is either typed in or scanned using OCR.
Card detail is gotten from [Binlist](https://binlist.net/)
App is still in development

## Features
* Clean architecture
* Databinding for binding data to views
* Dependency injection with Dagger 2
* Navigation component
* The Store 4 library by dropbox for effortless data fetch and In-memory caching for rotation. A good substitute for Repository and Viewmodel [Store 4](https://github.com/dropbox/Store/)

## Author
Ikem Nwodo

## License
This project is licensed under the Apache License 2.0 - See: http://www.apache.org/licenses/LICENSE-2.0.txt
